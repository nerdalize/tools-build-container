# build container

A container to build your go dependencies. You can include the output-binaries in another container. 

# Compiling
You can compile this container with `make build`. 


# Using
We assume you have build this project as container. 

Now imagine you have another project that builds a Docker container. You use the following Makefile to build it: 

```
IMAGE=dockerhub.com/myname/my-awesome-container
build: build-deps
        docker build -t $(IMAGE) -f Dockerfile .

build-deps:
        go get github.com/feliksik/yaml-go-json

```

Then you modify the Makefile as follows: 

```
IMAGE=dockerhub.com/myname/my-awesome-container

# we changed the build dependency
build: build-deps-in-container
        docker build -t $(IMAGE) -f Dockerfile .

# nothing changed here; 
# just note this must be called build-deps, as the target is mentioned in the docker command
build-deps:
        go get github.com/feliksik/yaml-go-json

# Note this target is standard for every project
BC_VERSION=0.1
build-deps-in-container:
        mkdir -p deps # assumes by build container
        docker run --rm -ti -v $(shell pwd):/project quay.io/nerdalize/build-container:$(BC_VERSION) bash -c 'cd /project; make build-deps'
```

And make sure your projects Dockerfile ADDs the binaries from deps/bin/ . 
