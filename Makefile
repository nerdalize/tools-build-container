IMAGE=quay.io/nerdalize/build-container

build:
	docker build -t $(IMAGE):`cat VERSION` .

publish: build
	docker push $(IMAGE):`cat VERSION`

