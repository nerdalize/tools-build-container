FROM ubuntu:14.04.3

MAINTAINER Eric Feliksik <e.feliksik@nerdalize.com>

ENV PATH $PATH:/usr/local/go/bin

# go build are output in /project/deps/bin ; /project is assumed to the volume mounted in your host
# project dir, so the binaries appear in the my-project/deps/bin/ dir on your host.
# You should probably add 'tmp' to your gitignore.
ENV GOPATH /project/deps
ENV GOROOT /usr/local/go


ENV GO_VERSION 1.5.1
RUN apt-get update && apt-get install -y build-essential unzip git python-pip python3-pip python-yaml python3-yaml curl

RUN pip3 install peru
RUN curl -L https://storage.googleapis.com/golang/go${GO_VERSION}.linux-amd64.tar.gz > /tmp/golang.tar.gz; tar -C /usr/local -xzf /tmp/golang.tar.gz; rm /tmp/golang.tar.gz

